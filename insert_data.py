import psycopg2
import csv

connection = psycopg2.connect("host='localhost' dbname='seven_classes' user='postgres' password='password'")
mycursor = connection.cursor()


def insert_students():
    with open('./Students.csv', newline='', encoding="utf8") as csvfile:
        spamreader = csv.reader(csvfile)
        for row in spamreader:

            sql = "INSERT INTO students(id, first_name, last_name, subjects, phone_number, enrolled_batch) VALUES ('%s', '%s', '%s' ,'%s', '%s', '%s' );" % (row[0], row[1], row[2], row[3], row[4], row[5])
            print(sql)
            try:
                mycursor.execute(sql)
                connection.commit()
            except:
                connection.rollback()


def insert_teachers():
    with open('./Teachers.csv', newline='', encoding="utf8") as csvfile:
        next(csvfile)
        spamreader = csv.reader(csvfile)
        for row in spamreader:
            sql = "INSERT INTO teachers(id, first_name, last_name, phone_number, teacher_subject) VALUES ('%s','%s', '%s', '%s', '%s' );" % (row[0], row[1], row[2], row[3], row[4])
            print(sql)
            try:
                mycursor.execute(sql)
                connection.commit()
            except:
                connection.rollback()


def insert_batch():
    with open('./Batch.csv', newline='', encoding="utf8") as csvfile:
        next(csvfile)
        spamreader = csv.reader(csvfile)
        for row in spamreader:
            sql = "INSERT INTO batch(batch_id, batch_name) VALUES ('%s','%s' );" % (row[0], row[1])
            print(sql)
            try:
                mycursor.execute(sql)
                connection.commit()

            except:
                connection.rollback()


def insert_subject():
    with open('./Subject.csv', newline='', encoding="utf8") as csvfile:
        next(csvfile)
        spamreader = csv.reader(csvfile)
        for row in spamreader:
            sql = "INSERT INTO subject(subject_id, subject_name) VALUES ('%s','%s' );" % (row[0], row[1])
            print(sql)

            try:
                mycursor.execute(sql)
                connection.commit()
            except:
                connection.rollback()


def insert_empty_slots():
    with open('./Empty_slots.csv', newline='', encoding="utf8") as csvfile:
        next(csvfile)
        spamreader = csv.reader(csvfile)
        for row in spamreader:
            sql = "INSERT INTO empty_slots(empty_slot_id, subject_id, teacher_id, day, start_at, end_at) VALUES ('%s','%s','%s','%s','%s','%s');" % (row[0], row[1], row[2], row[3], row[4], row[5])
            print(sql)

            try:
                mycursor.execute(sql)
                connection.commit()
            except:
                connection.rollback()


insert_students()
# insert_teachers()
# insert_batch()
# insert_subject()
# insert_empty_slots()
connection.close()
