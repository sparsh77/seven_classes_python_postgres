--
-- PostgreSQL database dump
--

-- Dumped from database version 12.7 (Ubuntu 12.7-0ubuntu0.20.04.1)
-- Dumped by pg_dump version 12.7 (Ubuntu 12.7-0ubuntu0.20.04.1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: batch; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.batch (
    batch_id integer NOT NULL,
    batch_name character varying
);


ALTER TABLE public.batch OWNER TO postgres;

--
-- Name: booked_slots; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.booked_slots (
    subject_id integer NOT NULL,
    teacher_id integer NOT NULL,
    day character varying(10) NOT NULL,
    start_at time without time zone NOT NULL,
    end_at time without time zone NOT NULL,
    student_id integer
);


ALTER TABLE public.booked_slots OWNER TO postgres;

--
-- Name: empty_slots; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.empty_slots (
    subject_id integer NOT NULL,
    teacher_id integer NOT NULL,
    day character varying(10) NOT NULL,
    start_at time without time zone NOT NULL,
    end_at time without time zone NOT NULL,
    empty_slot_id integer NOT NULL
);


ALTER TABLE public.empty_slots OWNER TO postgres;

--
-- Name: students; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.students (
    id integer NOT NULL,
    first_name character varying(50) NOT NULL,
    last_name character varying(50),
    subjects integer NOT NULL,
    phone_number numeric(10,0) NOT NULL,
    enrolled_batch integer
);


ALTER TABLE public.students OWNER TO postgres;

--
-- Name: students_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.students_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.students_id_seq OWNER TO postgres;

--
-- Name: students_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.students_id_seq OWNED BY public.students.id;


--
-- Name: subject; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.subject (
    subject_id integer NOT NULL,
    subject_name character varying
);


ALTER TABLE public.subject OWNER TO postgres;

--
-- Name: teachers; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.teachers (
    id integer NOT NULL,
    first_name character varying(50) NOT NULL,
    last_name character varying(50),
    phone_number numeric(10,0) NOT NULL,
    teacher_subject integer
);


ALTER TABLE public.teachers OWNER TO postgres;

--
-- Name: teachers_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.teachers_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.teachers_id_seq OWNER TO postgres;

--
-- Name: teachers_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.teachers_id_seq OWNED BY public.teachers.id;


--
-- Name: students id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.students ALTER COLUMN id SET DEFAULT nextval('public.students_id_seq'::regclass);


--
-- Name: teachers id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.teachers ALTER COLUMN id SET DEFAULT nextval('public.teachers_id_seq'::regclass);


--
-- Data for Name: batch; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.batch (batch_id, batch_name) FROM stdin;
1	CBSE
2	ICSE
3	State Board
4	Engineering
5	Medical
\.


--
-- Data for Name: booked_slots; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.booked_slots (subject_id, teacher_id, day, start_at, end_at, student_id) FROM stdin;
5	1	Friday	13:00:00	14:00:00	1
3	3	Wednesday	10:00:00	11:00:00	3
4	2	Thursday	09:00:00	10:00:00	2
2	4	Tuesday	17:00:00	18:00:00	2
\.


--
-- Data for Name: empty_slots; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.empty_slots (subject_id, teacher_id, day, start_at, end_at, empty_slot_id) FROM stdin;
1	5	Monday	10:00:00	11:00:00	1
2	4	Tuesday	09:00:00	10:00:00	2
3	3	Wednesday	13:00:00	14:00:00	3
4	2	Thursday	15:00:00	16:00:00	4
5	1	Friday	17:00:00	18:00:00	5
1	5	Monday	13:00:00	14:00:00	6
\.


--
-- Data for Name: students; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.students (id, first_name, last_name, subjects, phone_number, enrolled_batch) FROM stdin;
1	Student1	Student1	5	9955774433	5
2	Student2	Student2	4	8855221133	4
3	Student3	Student3	3	7788994455	3
4	Student4	Student4	2	9966332211	2
5	Student5	Student5	1	7744556688	1
6	Student6	Student6	1	8855446655	5
\.


--
-- Data for Name: subject; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.subject (subject_id, subject_name) FROM stdin;
1	Maths
2	Physics
3	Chemistry
4	Biology
5	General Knowledge
\.


--
-- Data for Name: teachers; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.teachers (id, first_name, last_name, phone_number, teacher_subject) FROM stdin;
1	Teacher1	Teacher1	2255884466	5
2	Teacher2	Teacher2	3366998877	4
3	Teacher3	Teacher3	8844112255	3
4	Teacher4	Teacher4	4455887799	2
5	Teacher5	Teacher5	5544668899	1
\.


--
-- Name: students_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.students_id_seq', 1, false);


--
-- Name: teachers_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.teachers_id_seq', 1, false);


--
-- Name: batch batch_batch_name_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.batch
    ADD CONSTRAINT batch_batch_name_key UNIQUE (batch_name);


--
-- Name: batch batch_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.batch
    ADD CONSTRAINT batch_pkey PRIMARY KEY (batch_id);


--
-- Name: empty_slots empty_slots_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.empty_slots
    ADD CONSTRAINT empty_slots_pkey PRIMARY KEY (empty_slot_id);


--
-- Name: students students_phone_number_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.students
    ADD CONSTRAINT students_phone_number_key UNIQUE (phone_number);


--
-- Name: students students_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.students
    ADD CONSTRAINT students_pkey PRIMARY KEY (id);


--
-- Name: subject subject_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.subject
    ADD CONSTRAINT subject_pkey PRIMARY KEY (subject_id);


--
-- Name: subject subject_subject_name_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.subject
    ADD CONSTRAINT subject_subject_name_key UNIQUE (subject_name);


--
-- Name: teachers teachers_phone_number_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.teachers
    ADD CONSTRAINT teachers_phone_number_key UNIQUE (phone_number);


--
-- Name: teachers teachers_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.teachers
    ADD CONSTRAINT teachers_pkey PRIMARY KEY (id);


--
-- Name: students fk_batch; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.students
    ADD CONSTRAINT fk_batch FOREIGN KEY (enrolled_batch) REFERENCES public.batch(batch_id);


--
-- Name: booked_slots fk_student_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.booked_slots
    ADD CONSTRAINT fk_student_id FOREIGN KEY (student_id) REFERENCES public.students(id);


--
-- Name: teachers fk_subject; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.teachers
    ADD CONSTRAINT fk_subject FOREIGN KEY (teacher_subject) REFERENCES public.subject(subject_id);


--
-- Name: students fk_subject; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.students
    ADD CONSTRAINT fk_subject FOREIGN KEY (subjects) REFERENCES public.subject(subject_id);


--
-- Name: empty_slots fk_subject_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.empty_slots
    ADD CONSTRAINT fk_subject_id FOREIGN KEY (subject_id) REFERENCES public.subject(subject_id);


--
-- Name: empty_slots fk_teacher_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.empty_slots
    ADD CONSTRAINT fk_teacher_id FOREIGN KEY (teacher_id) REFERENCES public.teachers(id);


--
-- PostgreSQL database dump complete
--

