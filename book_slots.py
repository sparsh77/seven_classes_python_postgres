import psycopg2
import csv

def read_empty_slots():
    conn = None
    try:
        conn = psycopg2.connect("host='localhost' dbname='seven_classes' user='postgres' password='password'")
        cur = conn.cursor()
        cur.execute("SELECT * FROM empty_slots")
        row = cur.fetchone()

        while row is not None:
            print("id  = ", row[5],)
            print("subject_id = ", row[0], )
            print("teacher_id = ", row[1])
            print("day  = ", row[2],)
            print("start_at  = ", row[3],)
            print("end_at  = ", row[4], "\n")

            row = cur.fetchone()

        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()

def get_empty_slot(id):
    conn = None
    try:
        conn = psycopg2.connect("host='localhost' dbname='seven_classes' user='postgres' password='password'")
        cur = conn.cursor()
        cur.execute("SELECT * FROM empty_slots WHERE empty_slot_id = %s", (id,))
        row = cur.fetchone()

        while row is not None:
            return row
            row = cur.fetchone()

        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()


def update_booked_slot(new_slot, student_id):
    conn = None
    try:
        conn = psycopg2.connect("host='localhost' dbname='seven_classes' user='postgres' password='password'")
        cur = conn.cursor()
        print("Here")
        cur.execute("INSERT INTO booked_slots(subject_id, teacher_id, day, start_at, end_at, student_id) VALUES ('%s','%s','%s','%s','%s', '%s' );" % (new_slot[0], new_slot[1], new_slot[2], new_slot[3], new_slot[4], student_id))
        conn.commit()
        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()


def delete_empty_slot(empty_slot_id):
    conn = None
    rows_deleted = 0
    try:
        conn = psycopg2.connect("host='localhost' dbname='seven_classes' user='postgres' password='password'")
        cur = conn.cursor()
        cur.execute("DELETE FROM empty_slots WHERE empty_slot_id = %s", (empty_slot_id,))
        rows_deleted = cur.rowcount
        conn.commit()
        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()

    return rows_deleted



if __name__ == '__main__':
    read_empty_slots()
    slot = int(input("Enter the slot id you want to book : "))
    new_slot = get_empty_slot(slot)
    student_id = int(input("Enter the student id : "))
    update_booked_slot(new_slot, student_id)
    delete_empty_slot(slot)

