import psycopg2


def main():
	conn_string = "host='localhost' dbname='seven_classes' user='postgres' password='password' "
	print("Connecting to database\n	->%s" % (conn_string))
	conn = psycopg2.connect(conn_string)
	cursor = conn.cursor()


if __name__ == "__main__":
	main()